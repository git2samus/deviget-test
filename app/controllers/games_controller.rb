class GamesController < ApplicationController
  def new_board
    board = Array.new(7)             # columns
    board.map! {|col| Array.new(6) } # pieces
  end

  def get_board
    session[:board] || new_board
  end

  def reset
    session[:board] = new_board
    redirect_to '/'
  end

  def play(player, column)
    board = get_board
    column = board[column]

    piece = (player == 'Player1')? 'X' : 'O'

    column.reverse!
    #TODO overflow error
    (0..6).each do |rownum|
      if column[rownum].nil?
        column[rownum] = piece
        break
      end
    end
    column.reverse!

    session[:board] = board
  end

  def player1
    @player = 'Player1'

    play @player, 0
    @board = get_board

    render :board
  end

  def player2
    @player = 'Player2'

    play @player, 0
    @board = get_board

    render :board
  end
end
