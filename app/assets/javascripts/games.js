$(function() {
  var scheme   = (window.document.location.protocol == 'https:')? "wss://" : "ws://";
  var uri      = scheme + window.document.location.host + "/";
  var ws       = new WebSocket(uri);

  ws.onmessage = function(message) {
    var data = JSON.parse(message.data);
  };

  var activeColumn = 0;
  var drawMarker = function() {
    var headers = $('#board th');

    headers.text('');
    $(headers[activeColumn]).text('V');
  };
  drawMarker();

  $(document).on("keydown", function(ev) {
    switch (ev.key) {
      case "Left":
        activeColumn = (activeColumn + 6) % 7;
        break;
      case "Right":
        activeColumn = (activeColumn + 1) % 7;
        break;
      case "Enter":
        break;
    };

    drawMarker();
  });
});
